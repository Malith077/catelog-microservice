using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Play.Catelog.Service.DTOs;
using Play.Catelog.Service.Entities;
using Play.Catelog.Service.Repositories;

namespace Play.Catelog.Service.Controllers
{
    [ApiController]
    [Route("items")]
    public class ItemsController : ControllerBase
    {
        private static readonly List<ItemDTO> items = new()
        {
            new ItemDTO(Guid.NewGuid(), "Potion", "Item 1 description", 10.0m, DateTimeOffset.UtcNow),
            new ItemDTO(Guid.NewGuid(), "Super Potion", "Item 2 description", 20.0m, DateTimeOffset.UtcNow),
            new ItemDTO(Guid.NewGuid(), "Hyper Potion", "Item 3 description", 30.0m, DateTimeOffset.UtcNow),
        };

        private readonly ItemsRepository itemsRepository = new();

        [HttpGet]
        public async Task<IEnumerable<ItemDTO>> GetAsync()
        {
            return (await itemsRepository.GetAllAsync()).Select(x => x.AsDTO());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ItemDTO>> GetByIdAsync(Guid id)
        {
            var item = await itemsRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return item.AsDTO();
        }

        [HttpPost]
        public async Task<ActionResult<ItemDTO>> PostAsync(CreateItemDTO createItemDTO)
        {
            var item = new Item
            {
                Name = createItemDTO.Name,
                Description = createItemDTO.Description,
                Price = createItemDTO.Price,
                CreatedDate = DateTimeOffset.UtcNow
            };

            await itemsRepository.CreateAsync(item);

            return CreatedAtAction(nameof(GetByIdAsync), new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(Guid Id, UpdateItemDTO updateItemDTO)
        {
            var existing = await itemsRepository.GetAsync(Id);

            if (existing == null)
            {
                return NotFound();
            }

            {
                existing.Name = updateItemDTO.Name;
                existing.Description = updateItemDTO.Description;
                existing.Price = updateItemDTO.Price;
            }

            await itemsRepository.UpdateAsync(existing);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid Id)
        {
            var item = await itemsRepository.GetAsync(Id);

            if (item == null)
            {
                return NotFound();
            }

            await itemsRepository.RemoveAsync(Id);

            return NoContent();
        }
    }

}